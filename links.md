# Links úteis

## Microsoft Office 365
- Página inicial - https://www.office.com/
- Cadastro - https://www.microsoft.com/pt-br/education/products/office
- Recuperação de senha - https://passwordreset.microsoftonline.com/
- Teams - https://teams.microsoft.com/
- OneDrive - https://onedrive.live.com/about/pt-br/signin
- Política de privacidade - https://privacy.microsoft.com/pt-br/privacystatement
- Suporte - https://support.microsoft.com/pt-br/home/contact

## IFRS
- Regulamento de e-mail institucinal - https://ifrs.edu.br/tecnologia-da-informacao/documentos/regulamento-de-utilizacao-do-e-mail-institucional/
- Tutorial original (DTI) - [Tutorial-de-Acesso-ao-Office-Education-365-e-MS-TEAMS](_media/Tutorial-de-Acesso-ao-Office-Education-365-e-MS-TEAMS.pdf ':ignore') (PDF)

## IFRS - Campus Canoas
- Coordenadoria de T.I. - cti@canoas.ifrs.edu.br

