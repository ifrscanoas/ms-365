# Teams

Microsoft Teams é um aplicativo de colaboração criado para trabalho híbrido para que você e sua equipe permaneçam informados, organizados e conectados tudo em um só lugar.

Explore como Teams pode ajudar você e seus colegas a se reunirem, independentemente de onde você está:

- Chat - Mensagem de alguém ou grupo para falar sobre trabalho, projetos ou apenas por diversão.

- Teams - Crie uma equipe e canais para reunir pessoas e trabalhar em espaços focados com conversas e arquivos.

- Calendário - Conexão com pessoas antes, durante e depois de uma reunião para que a preparação e o acompanhamento sejam fáceis de encontrar.

Saiba mais:

https://support.microsoft.com/pt-br/office/introdu%C3%A7%C3%A3o-ao-microsoft-teams-b98d533f-118e-4bae-bf44-3df2470c2b12?wt.mc_id=otc_microsoft_teams#

## Acesso

Microsoft Teams pode ser acessado pelo acesso geral do Microsoft Office 365 https://www.office.com/ ou diretamente pelo endereço https://teams.microsoft.com/

![Tela 6](../_media/6.png)

## Reuniões

Para realizar uma reunião online no Teams acesse o menu "Calendário".

> Existem duas formas de criar uma reunião, uma Reunião Instantânea ou Reunião Agendada.

### Reunião Instantânea

No calendário clique em "Reunir agora", neste momento você pode definir o nome da reunião e copiar o link de acesso a ser compartilhado para os participantes.

![Tela 7](../_media/7.png)

Ao clicar em "Iniciar reunião" você será direcionado a uma tela onde poderá revisar a suas configurações de áudio e vídeo. Feita as configurações clique em "Reunir agora"

![Tela 8](../_media/8.png)

Na tela seguinte você poderá, novamente, copiar o link da reunião ou enviá-lo por e-mail aos demais participantes.

![Tela 9](../_media/9.png)

### Reunião Agendada

No calendário clique em "Nova reunião".

![Tela 10](../_media/10.png)

Na tela seguinte você pode definir os detalhes da reunião, como título, participantes, dia e horário de início e fim, bem como os detalhes da reunião que serão enviados por e-mail aos participantes, juntamente com o link para acessar a reunião.

![Tela 11](../_media/11.png)

Após enviar o convite, a reunião ficará registrada no calendário do Teams, onde é possível obter o link de acesso, entrar ou editar a reunião.

### Menus de utilização durante reunião

Os menus disponibilizados durante uma reunião são:

![Tela 12](../_media/12.png)

1. Botão para sair da reunião;
2. Botão para exibir a lista de participantes;
3. Botão para levantar a mão;
4. Botão de **mais opções** (vide imagem abaixo);
5. Botão para compartilhar a tela ou uma apresentação de slides;
6. Botão para fechar ou abrir o microfone;
7. Botão para fechar ou abrir a câmera;
8. Tempo de duração da reunião.

Botão **Mais opções** expandido:

![Tela 13](../_media/13.png)

Após finalizada a reunião, de volta ao Teams na tela relacionada aquela reunião será possível obter o relatório de presença e acessar a gravação da reunião, caso ela tenha sido realizada através do botão **Mais opções** conforme a instrução seguinte.


![Tela 14](../_media/14.png)

### Gravação

!> Atenção: a gravação fica armazenada por 30 dias, após este período a gravação é deletada automaticamente


A gravação da reunião pode ser iniciada através do botão **Mais opções** > **Iniciar gravação** e após finalizada é possível acessar a gravação no relatório da reunião conforme imagem acima ou no OneDrive.

A gravação ficará salva na pasta "Gravações" criada automaticamente no OneDrive do usuário que criou a reunião. O OneDrive pode ser acessado pelo acesso geral do Microsoft Office 365 https://www.office.com/ ou diretamente pelo endereço https://onedrive.live.com/about/pt-br/signin


### Mais informações 

Para mais informações e tutoriais sobre o Teams, acesse: 

https://support.microsoft.com/pt-br/office/treinamento-em-v%C3%ADdeo-do-microsoft-teams-4f108e54-240b-4351-8084-b1089f0d21d7