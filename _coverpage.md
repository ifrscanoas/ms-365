<!-- _coverpage.md -->

![logo](_media/0.png ':size=45%')

> Microsoft Office 365 Online

- Acesso as ferramentas online, Word, Excel, Power Point, Teams, OneDrive e etc.
- Acessível de qualquer navegador.

[Introdução](README)
[Cadastro](cadastro)
[Links úteis](links)


<!-- ![color](#eb3d01) -->