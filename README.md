# Introdução

## Público alvo

As ferramentas do Microsoft Office 365 para Educação são acessíveis por toda a comunidade acadêmica, discentes, docentes e técnicos administrativos que possuam uma conta de e-mail institucional são elegíveis para se cadastrar.

## O que é o Microsoft Office 365 para Educação

É um pacote de ferramentas de escritório que disponibiliza para a comunidade acadêmica o acesso gratuito das principais ferramentas da Microsoft, como Word, Excel, Power Point, Teams, OneDrive e etc.

## Limitações

- O acesso as ferramentas se dá somente via navegador, o **pacote Microsoft Office 365 para Educação não dispõe das ferramentas instaláveis no computador**.

- O aplicativo Microsoft Outlook e Exchange esta desativado, pois o gerenciador de e-mails do IFRS é fornecido pelo Google.

- Ao se cadastrar no Microsoft Office 365 para Educação, o usuário cria uma senha específica para acesso aos serviços fornecidos pela Microsoft que não possui qualquer integração com os sistemas fornecidos pelo *campus* Canoas, ou seja, ao esquecer a senha de acesso você necessita utilizar os recursos de recuperação de senha fornecidos pela Microsoft.

- O acesso as ferramentas utilizando o e-mail institucional é garantido enquanto a conta de e-mail é ativa, o IFRS *campus* Canoas se dá o direito de revogar o acesso em caso de desligamento da instituição seguindo a [RESOLUÇÃO Nº 010, DE 05 DE FEVEREIRO DE 2021](https://ifrs.edu.br/tecnologia-da-informacao/documentos/regulamento-de-utilizacao-do-e-mail-institucional/) do IFRS.


## Política de privacidade

Ao se cadastrar e utilizar as ferramentas do pacote Microsoft Office 365 para Educação você estará sujeito a [Política de Privacidade da Microsoft](https://privacy.microsoft.com/pt-br/privacystatement).