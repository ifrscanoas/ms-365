# Cadastro

Passo 1 - Para o primeiro acesso, você deve ser cadastrar no site da Microsoft destinado para instituições de educação, acesse o endereço abaixo e no campo apresentado insira seu e-mail institucional:

https://www.microsoft.com/pt-br/education/products/office

![Tela 1](_media/1.png)

#### Lembrete:

> Para os servidores do *campus* Canoas o e-mail institucional é nome.sobrenome@canoas.ifrs.edu.br. 

> Para os discentes o e-mail institucional é o número de matrícula com o domínio @aluno.canoas.ifrs.edu.br, **exemplo 000001@aluno.canoas.ifrs.edu.br**.

Passo 2 - Na próxima janela escolha seu segmento dentro da instituição.

![Tela 2](_media/2.png)

Passo 3 - Complete o cadastro, crie uma senha para acesso específico aos serviços da Microsoft e **insira o código de verificação que você recebeu no e-mail institucional**.

![Tela 3](_media/3.png)

Passo 4 (Opcional) - Esta é uma etapa opcional que visa ampliar a segurança do seu acesso, informe seu país e número de celular ou clique em **Pular configuração** para ignorar esta etapa.

![Tela 4](_media/4.png)

Realizada as etapas você terá acesso via navegador a todos os aplicativos fornecidos pela Microsoft, também é possível acessar as ferramentas diretamente pelo endereço abaixo:

https://www.office.com/


## Aplicativos

No menu à esquerda é possível visualizar e acessar todos os aplicativos.

![Tela 5](_media/5.png)